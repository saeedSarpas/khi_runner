#!/bin/bash
#
# Usage:
# ./runner.sh 1

wd=`pwd`

mkdir -p ./Rhyme/build
cd ./Rhyme/build && cmake .. && make && ctest --output-on-failure
cd ${wd}

if [ $1 -lt 6 ]; then
  bash -c "./Rhyme/build/rhyme Mandelker_${1}.conf"
else
  bash -c "./Rhyme/build/rhyme Mandelker_${1}_non_eigenmode.conf"
fi
